﻿using System;
using System.Windows.Forms;
using WinAllocator.Example1;
using WinAllocator.Example2;

namespace WinAllocator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ButtonExample1_Click(object sender, EventArgs e)
        {
            NavigateToForm(new ExampleForm1());
        }

        private void ButtonExample2_Click(object sender, EventArgs e)
        {
            NavigateToForm(new ExampleForm2());
        }

        private void NavigateToForm(Form form)
        {
            form.FormClosed += AutoClose;
            form.Show();
            this.Hide();
        }

        private void AutoClose(object sender, EventArgs e)
        {
            this.Close();
        }        
    }
}
