﻿namespace WinAllocator.Example1
{
    partial class ExampleForm1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAlloc = new System.Windows.Forms.Button();
            this.buttonFree = new System.Windows.Forms.Button();
            this.buttonSelectPage = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnParamName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnParamValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPageCount = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelType = new System.Windows.Forms.Label();
            this.labelProtect = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.Label();
            this.labelRegionSize = new System.Windows.Forms.Label();
            this.labelAllocationProtect = new System.Windows.Forms.Label();
            this.labelAllocationBase = new System.Windows.Forms.Label();
            this.labelBaseAddress = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxPaging = new System.Windows.Forms.GroupBox();
            this.buttonTryWriteBytesToPage = new System.Windows.Forms.Button();
            this.buttonMakePrivate = new System.Windows.Forms.Button();
            this.labelSelectedPage = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.labelPageCount = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBoxSelectedPage = new System.Windows.Forms.GroupBox();
            this.labelStateSelectedPage = new System.Windows.Forms.Label();
            this.labelAllocationProtectSelectedPage = new System.Windows.Forms.Label();
            this.labelTypeSelectedPage = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelProtectSelectedPage = new System.Windows.Forms.Label();
            this.labelBaseAddressSelectedPage = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxPaging.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            this.groupBoxSelectedPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAlloc
            // 
            this.buttonAlloc.Location = new System.Drawing.Point(15, 71);
            this.buttonAlloc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonAlloc.Name = "buttonAlloc";
            this.buttonAlloc.Size = new System.Drawing.Size(157, 32);
            this.buttonAlloc.TabIndex = 0;
            this.buttonAlloc.Text = "Alloc";
            this.buttonAlloc.UseVisualStyleBackColor = true;
            this.buttonAlloc.Click += new System.EventHandler(this.ButtonAlloc_Click);
            // 
            // buttonFree
            // 
            this.buttonFree.Location = new System.Drawing.Point(15, 109);
            this.buttonFree.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonFree.Name = "buttonFree";
            this.buttonFree.Size = new System.Drawing.Size(157, 33);
            this.buttonFree.TabIndex = 1;
            this.buttonFree.Text = "Free";
            this.buttonFree.UseVisualStyleBackColor = true;
            this.buttonFree.Click += new System.EventHandler(this.ButtonFree_Click);
            // 
            // buttonSelectPage
            // 
            this.buttonSelectPage.Location = new System.Drawing.Point(307, 53);
            this.buttonSelectPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonSelectPage.Name = "buttonSelectPage";
            this.buttonSelectPage.Size = new System.Drawing.Size(143, 32);
            this.buttonSelectPage.TabIndex = 2;
            this.buttonSelectPage.Text = "Select page";
            this.buttonSelectPage.UseVisualStyleBackColor = true;
            this.buttonSelectPage.Click += new System.EventHandler(this.ButtonSelectPage_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Location = new System.Drawing.Point(14, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(277, 427);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "System Info";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnParamName,
            this.columnParamValue});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(6, 25);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(265, 395);
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnParamName
            // 
            this.columnParamName.Text = "Parametr";
            this.columnParamName.Width = 117;
            // 
            // columnParamValue
            // 
            this.columnParamValue.Text = "Value";
            this.columnParamValue.Width = 128;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.textBoxPageCount);
            this.groupBox2.Controls.Add(this.buttonAlloc);
            this.groupBox2.Controls.Add(this.buttonFree);
            this.groupBox2.Location = new System.Drawing.Point(297, 286);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 154);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Memory allocation";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(11, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 30);
            this.label1.TabIndex = 3;
            this.label1.Text = "Pages:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPageCount
            // 
            this.textBoxPageCount.Location = new System.Drawing.Point(82, 33);
            this.textBoxPageCount.Name = "textBoxPageCount";
            this.textBoxPageCount.Size = new System.Drawing.Size(90, 30);
            this.textBoxPageCount.TabIndex = 2;
            this.textBoxPageCount.Text = "5";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelType);
            this.groupBox3.Controls.Add(this.labelProtect);
            this.groupBox3.Controls.Add(this.labelState);
            this.groupBox3.Controls.Add(this.labelRegionSize);
            this.groupBox3.Controls.Add(this.labelAllocationProtect);
            this.groupBox3.Controls.Add(this.labelAllocationBase);
            this.groupBox3.Controls.Add(this.labelBaseAddress);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(297, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(467, 132);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Memory basic information";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(328, 70);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(17, 23);
            this.labelType.TabIndex = 13;
            this.labelType.Text = "-";
            // 
            // labelProtect
            // 
            this.labelProtect.AutoSize = true;
            this.labelProtect.Location = new System.Drawing.Point(328, 48);
            this.labelProtect.Name = "labelProtect";
            this.labelProtect.Size = new System.Drawing.Size(17, 23);
            this.labelProtect.TabIndex = 12;
            this.labelProtect.Text = "-";
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(328, 25);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(17, 23);
            this.labelState.TabIndex = 11;
            this.labelState.Text = "-";
            // 
            // labelRegionSize
            // 
            this.labelRegionSize.AutoSize = true;
            this.labelRegionSize.Location = new System.Drawing.Point(164, 94);
            this.labelRegionSize.Name = "labelRegionSize";
            this.labelRegionSize.Size = new System.Drawing.Size(17, 23);
            this.labelRegionSize.TabIndex = 10;
            this.labelRegionSize.Text = "-";
            // 
            // labelAllocationProtect
            // 
            this.labelAllocationProtect.AutoSize = true;
            this.labelAllocationProtect.Location = new System.Drawing.Point(164, 71);
            this.labelAllocationProtect.Name = "labelAllocationProtect";
            this.labelAllocationProtect.Size = new System.Drawing.Size(17, 23);
            this.labelAllocationProtect.TabIndex = 9;
            this.labelAllocationProtect.Text = "-";
            // 
            // labelAllocationBase
            // 
            this.labelAllocationBase.AutoSize = true;
            this.labelAllocationBase.Location = new System.Drawing.Point(164, 49);
            this.labelAllocationBase.Name = "labelAllocationBase";
            this.labelAllocationBase.Size = new System.Drawing.Size(17, 23);
            this.labelAllocationBase.TabIndex = 8;
            this.labelAllocationBase.Text = "-";
            // 
            // labelBaseAddress
            // 
            this.labelBaseAddress.AutoSize = true;
            this.labelBaseAddress.Location = new System.Drawing.Point(164, 26);
            this.labelBaseAddress.Name = "labelBaseAddress";
            this.labelBaseAddress.Size = new System.Drawing.Size(17, 23);
            this.labelBaseAddress.TabIndex = 7;
            this.labelBaseAddress.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(253, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 23);
            this.label8.TabIndex = 6;
            this.label8.Text = "Type:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(253, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 23);
            this.label7.TabIndex = 5;
            this.label7.Text = "Protect:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(253, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 23);
            this.label6.TabIndex = 4;
            this.label6.Text = "State:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 23);
            this.label5.TabIndex = 3;
            this.label5.Text = "RegionSize:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 23);
            this.label4.TabIndex = 2;
            this.label4.Text = "AllocationProtect:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 23);
            this.label3.TabIndex = 1;
            this.label3.Text = "AllocationBase:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "BaseAddress:";
            // 
            // groupBoxPaging
            // 
            this.groupBoxPaging.Controls.Add(this.buttonTryWriteBytesToPage);
            this.groupBoxPaging.Controls.Add(this.buttonMakePrivate);
            this.groupBoxPaging.Controls.Add(this.labelSelectedPage);
            this.groupBoxPaging.Controls.Add(this.label12);
            this.groupBoxPaging.Controls.Add(this.numericUpDown);
            this.groupBoxPaging.Controls.Add(this.labelPageCount);
            this.groupBoxPaging.Controls.Add(this.label9);
            this.groupBoxPaging.Controls.Add(this.buttonSelectPage);
            this.groupBoxPaging.Enabled = false;
            this.groupBoxPaging.Location = new System.Drawing.Point(297, 151);
            this.groupBoxPaging.Name = "groupBoxPaging";
            this.groupBoxPaging.Size = new System.Drawing.Size(467, 129);
            this.groupBoxPaging.TabIndex = 6;
            this.groupBoxPaging.TabStop = false;
            this.groupBoxPaging.Text = "Paging";
            // 
            // buttonTryWriteBytesToPage
            // 
            this.buttonTryWriteBytesToPage.Location = new System.Drawing.Point(15, 85);
            this.buttonTryWriteBytesToPage.Name = "buttonTryWriteBytesToPage";
            this.buttonTryWriteBytesToPage.Size = new System.Drawing.Size(290, 32);
            this.buttonTryWriteBytesToPage.TabIndex = 9;
            this.buttonTryWriteBytesToPage.Text = "Try write bytes to page";
            this.buttonTryWriteBytesToPage.UseVisualStyleBackColor = true;
            this.buttonTryWriteBytesToPage.Click += new System.EventHandler(this.ButtonTryWriteBytesToPage_Click);
            // 
            // buttonMakePrivate
            // 
            this.buttonMakePrivate.Location = new System.Drawing.Point(307, 85);
            this.buttonMakePrivate.Name = "buttonMakePrivate";
            this.buttonMakePrivate.Size = new System.Drawing.Size(143, 32);
            this.buttonMakePrivate.TabIndex = 8;
            this.buttonMakePrivate.Text = "Protect page";
            this.buttonMakePrivate.UseVisualStyleBackColor = true;
            this.buttonMakePrivate.Click += new System.EventHandler(this.ButtonMakePrivate_Click);
            // 
            // labelSelectedPage
            // 
            this.labelSelectedPage.AutoSize = true;
            this.labelSelectedPage.Location = new System.Drawing.Point(138, 49);
            this.labelSelectedPage.Name = "labelSelectedPage";
            this.labelSelectedPage.Size = new System.Drawing.Size(19, 23);
            this.labelSelectedPage.TabIndex = 7;
            this.labelSelectedPage.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 23);
            this.label12.TabIndex = 6;
            this.label12.Text = "Selected page:";
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(307, 22);
            this.numericUpDown.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(143, 30);
            this.numericUpDown.TabIndex = 5;
            // 
            // labelPageCount
            // 
            this.labelPageCount.AutoSize = true;
            this.labelPageCount.Location = new System.Drawing.Point(137, 26);
            this.labelPageCount.Name = "labelPageCount";
            this.labelPageCount.Size = new System.Drawing.Size(19, 23);
            this.labelPageCount.TabIndex = 4;
            this.labelPageCount.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 23);
            this.label9.TabIndex = 3;
            this.label9.Text = "Count:";
            // 
            // groupBoxSelectedPage
            // 
            this.groupBoxSelectedPage.Controls.Add(this.labelStateSelectedPage);
            this.groupBoxSelectedPage.Controls.Add(this.labelAllocationProtectSelectedPage);
            this.groupBoxSelectedPage.Controls.Add(this.labelTypeSelectedPage);
            this.groupBoxSelectedPage.Controls.Add(this.label17);
            this.groupBoxSelectedPage.Controls.Add(this.label11);
            this.groupBoxSelectedPage.Controls.Add(this.labelProtectSelectedPage);
            this.groupBoxSelectedPage.Controls.Add(this.labelBaseAddressSelectedPage);
            this.groupBoxSelectedPage.Controls.Add(this.label14);
            this.groupBoxSelectedPage.Controls.Add(this.label10);
            this.groupBoxSelectedPage.Controls.Add(this.label15);
            this.groupBoxSelectedPage.Location = new System.Drawing.Point(496, 286);
            this.groupBoxSelectedPage.Name = "groupBoxSelectedPage";
            this.groupBoxSelectedPage.Size = new System.Drawing.Size(274, 154);
            this.groupBoxSelectedPage.TabIndex = 7;
            this.groupBoxSelectedPage.TabStop = false;
            this.groupBoxSelectedPage.Text = "Selected page";
            // 
            // labelStateSelectedPage
            // 
            this.labelStateSelectedPage.AutoSize = true;
            this.labelStateSelectedPage.Location = new System.Drawing.Point(159, 49);
            this.labelStateSelectedPage.Name = "labelStateSelectedPage";
            this.labelStateSelectedPage.Size = new System.Drawing.Size(17, 23);
            this.labelStateSelectedPage.TabIndex = 14;
            this.labelStateSelectedPage.Text = "-";
            // 
            // labelAllocationProtectSelectedPage
            // 
            this.labelAllocationProtectSelectedPage.AutoSize = true;
            this.labelAllocationProtectSelectedPage.Location = new System.Drawing.Point(159, 72);
            this.labelAllocationProtectSelectedPage.Name = "labelAllocationProtectSelectedPage";
            this.labelAllocationProtectSelectedPage.Size = new System.Drawing.Size(17, 23);
            this.labelAllocationProtectSelectedPage.TabIndex = 15;
            this.labelAllocationProtectSelectedPage.Text = "-";
            // 
            // labelTypeSelectedPage
            // 
            this.labelTypeSelectedPage.AutoSize = true;
            this.labelTypeSelectedPage.Location = new System.Drawing.Point(159, 118);
            this.labelTypeSelectedPage.Name = "labelTypeSelectedPage";
            this.labelTypeSelectedPage.Size = new System.Drawing.Size(17, 23);
            this.labelTypeSelectedPage.TabIndex = 17;
            this.labelTypeSelectedPage.Text = "-";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 23);
            this.label17.TabIndex = 14;
            this.label17.Text = "AllocationProtect:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 23);
            this.label11.TabIndex = 2;
            this.label11.Text = "State:";
            // 
            // labelProtectSelectedPage
            // 
            this.labelProtectSelectedPage.AutoSize = true;
            this.labelProtectSelectedPage.Location = new System.Drawing.Point(159, 95);
            this.labelProtectSelectedPage.Name = "labelProtectSelectedPage";
            this.labelProtectSelectedPage.Size = new System.Drawing.Size(17, 23);
            this.labelProtectSelectedPage.TabIndex = 16;
            this.labelProtectSelectedPage.Text = "-";
            // 
            // labelBaseAddressSelectedPage
            // 
            this.labelBaseAddressSelectedPage.AutoSize = true;
            this.labelBaseAddressSelectedPage.Location = new System.Drawing.Point(159, 26);
            this.labelBaseAddressSelectedPage.Name = "labelBaseAddressSelectedPage";
            this.labelBaseAddressSelectedPage.Size = new System.Drawing.Size(17, 23);
            this.labelBaseAddressSelectedPage.TabIndex = 1;
            this.labelBaseAddressSelectedPage.Text = "-";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 23);
            this.label14.TabIndex = 15;
            this.label14.Text = "Type:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 23);
            this.label10.TabIndex = 0;
            this.label10.Text = "Base address:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 95);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 23);
            this.label15.TabIndex = 14;
            this.label15.Text = "Protect:";
            // 
            // Example1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 460);
            this.Controls.Add(this.groupBoxSelectedPage);
            this.Controls.Add(this.groupBoxPaging);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Example1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Example1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxPaging.ResumeLayout(false);
            this.groupBoxPaging.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            this.groupBoxSelectedPage.ResumeLayout(false);
            this.groupBoxSelectedPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAlloc;
        private System.Windows.Forms.Button buttonFree;
        private System.Windows.Forms.Button buttonSelectPage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnParamName;
        private System.Windows.Forms.ColumnHeader columnParamValue;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPageCount;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelProtect;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Label labelRegionSize;
        private System.Windows.Forms.Label labelAllocationProtect;
        private System.Windows.Forms.Label labelAllocationBase;
        private System.Windows.Forms.Label labelBaseAddress;
        private System.Windows.Forms.GroupBox groupBoxPaging;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelPageCount;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.GroupBox groupBoxSelectedPage;
        private System.Windows.Forms.Label labelBaseAddressSelectedPage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelStateSelectedPage;
        private System.Windows.Forms.Label labelAllocationProtectSelectedPage;
        private System.Windows.Forms.Label labelTypeSelectedPage;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelProtectSelectedPage;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelSelectedPage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonMakePrivate;
        private System.Windows.Forms.Button buttonTryWriteBytesToPage;
    }
}

