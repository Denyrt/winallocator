﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using WinAllocator.Core;
using WinAllocator.Core.Enums;
using static WinAllocator.Core.WinApiExtensions;

namespace WinAllocator.Example1
{

    public partial class ExampleForm1 : Form
    {
        private SYSTEM_INFO _sys_info;
        private MEMORY_BASIC_INFORMATION _mbi;
        private IntPtr handle;

        public ExampleForm1()
        {           
            InitializeComponent();
            GetSystemInfo(ref _sys_info);
            FillFromSysInfo(in _sys_info);
            FillMemoryBasicInfo(MEMORY_BASIC_INFORMATION.Empty);
        }

        private void FillFromSysInfo(in SYSTEM_INFO sysInfo) 
        {
            listView1.Items.Clear();

            var pageSizeItem = listView1.Items.Add("page size");
            pageSizeItem.SubItems.Add($"{sysInfo.dwPageSize} bytes");            
        }

        private void FillMemoryBasicInfo(in MEMORY_BASIC_INFORMATION mbi)
        {
            labelBaseAddress.Text = mbi.BaseAddress.ToAddressString();
            labelAllocationBase.Text = mbi.AllocationBase.ToAddressString();
            labelAllocationProtect.Text = mbi.AllocationProtect.ToString();
            labelRegionSize.Text = mbi.RegionSize.ToString();
            labelState.Text = mbi.State.ToString();
            labelProtect.Text = mbi.Protect.ToString();
            labelType.Text = mbi.Type.ToString();
        }

        private void FillSelectedPageInfo(in MEMORY_BASIC_INFORMATION pageMbi)
        {
            labelBaseAddressSelectedPage.Text = pageMbi.BaseAddress.ToAddressString();
            labelStateSelectedPage.Text = pageMbi.State.ToString();
            labelAllocationProtectSelectedPage.Text = pageMbi.AllocationProtect.ToString();
            labelProtectSelectedPage.Text = pageMbi.Protect.ToString();
            labelTypeSelectedPage.Text = pageMbi.Type.ToString();
        }

        private void RefreshPagingBox()
        {
            numericUpDown.Value = 0;
            numericUpDown.Maximum = 0;
            groupBoxPaging.Enabled = false;
        }

        private unsafe void ButtonAlloc_Click(object sender, EventArgs e)
        {
            if (!uint.TryParse(textBoxPageCount.Text, out var pageCount) || pageCount == 0)
            {
                MessageBox.Show("Incorrect pages count.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            handle = VirtualAlloc(handle, pageCount * _sys_info.dwPageSize, AllocationType.RESERVE, MemoryProtection.NOACCESS);
            
            if (handle != IntPtr.Zero)
            {
                MessageBox.Show("Success alloc", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                
                VirtualQuery(handle, ref _mbi, (uint)Marshal.SizeOf(typeof(MEMORY_BASIC_INFORMATION)));
                FillMemoryBasicInfo(_mbi);
                
                labelPageCount.Text = pageCount.ToString();
                
                numericUpDown.Value = 0;
                numericUpDown.Maximum = pageCount;
                groupBoxPaging.Enabled = true;
            }
            else
            {
                FillSelectedPageInfo(in MEMORY_BASIC_INFORMATION.Empty);
                RefreshPagingBox();                
                MessageBox.Show($"Alloc failed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ButtonFree_Click(object sender, EventArgs e)
        { 
            if (VirtualFree(handle, 0, AllocationType.RELEASE))
            {
                labelSelectedPage.Text = "0";
                FillSelectedPageInfo(in MEMORY_BASIC_INFORMATION.Empty);
                RefreshPagingBox();
                MessageBox.Show("Ok Free");
            }
            else
            {
                int error = Marshal.GetLastWin32Error();
                MessageBox.Show($"Not Free.\nWin32 Error: {error}");
            }
            var size = (uint)Marshal.SizeOf(_mbi);
            VirtualQuery(handle, ref _mbi, (uint)Marshal.SizeOf(_mbi));
            FillMemoryBasicInfo(_mbi);
        }

        private void ButtonSelectPage_Click(object sender, EventArgs e)
        {            
            var page = SelectPage((int)numericUpDown.Value);            
            var pageMbi = new MEMORY_BASIC_INFORMATION();
            VirtualQuery(page, ref pageMbi, (uint)Marshal.SizeOf(pageMbi));
            FillSelectedPageInfo(pageMbi);
            labelSelectedPage.Text = numericUpDown.Value.ToString();

            // Success
            if (page != IntPtr.Zero) return;
            
            // Trying get next page.
            bool selectNextPage;
            
            do
            {
                selectNextPage = false;
                MessageBox.Show("Page selecting failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);                                   

                var result = MessageBox.Show("Would you like select next page?", "Next page", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (numericUpDown.Value == numericUpDown.Maximum)
                    {
                        MessageBox.Show("It was last page. You have no page to select it.", "Page Limit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    
                    ++numericUpDown.Value;
                    page = SelectPage((int)numericUpDown.Value);
                    VirtualQuery(page, ref pageMbi, (uint)Marshal.SizeOf(pageMbi));
                    FillSelectedPageInfo(pageMbi);
                    labelSelectedPage.Text = numericUpDown.Value.ToString();

                    selectNextPage = page == IntPtr.Zero;                    
                }
            }
            while (selectNextPage);
        }

        /// <summary>
        /// Select currect page.
        /// </summary>
        /// <param name="numberOfPage"> Number of page. Starts from 0. </param>
        /// <returns> Returns pointer of page. Result will be IntPtr.Zero if attempt is failed. </returns>
        private IntPtr SelectPage(int numberOfPage)
        {            
            var ptr = IntPtr.Add(handle, (int)_sys_info.dwPageSize * numberOfPage);
            return VirtualAlloc(ptr, _sys_info.dwPageSize, AllocationType.COMMIT, MemoryProtection.READWRITE);
        }

        /// <summary>
        /// Protected page as readonly.
        /// </summary>
        /// <param name="numberOfPage"> Number of page. Starts from 0. </param>
        /// <param name="oldProtection"> Old protection of page. </param>
        /// <param name="pageMbi"> Memory basic information of protected page. </param>
        /// <returns></returns>
        private bool ProtectPage(int numberOfPage, ref MemoryProtection oldProtection, out MEMORY_BASIC_INFORMATION pageMbi)
        {
            var ptr = IntPtr.Add(handle, (int)_sys_info.dwPageSize * numberOfPage);            
            var protect = VirtualProtect(ptr, (uint)Marshal.SizeOf(typeof(MEMORY_BASIC_INFORMATION)), MemoryProtection.READONLY, ref oldProtection);
            
            pageMbi = default;
            VirtualQuery(ptr, ref pageMbi, (uint)Marshal.SizeOf(pageMbi));

            return protect;
        }

        private void ButtonMakePrivate_Click(object sender, EventArgs e)
        {
            MemoryProtection old = default;
            
            if (ProtectPage((int)numericUpDown.Value, ref old, out var pageMbi))
            {                
                FillSelectedPageInfo(in pageMbi);
                MessageBox.Show("Page success protected.", "Protection", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                var error = Marshal.GetLastWin32Error();
                MessageBox.Show($"Failed protection.\nWin32 Error: {error}.", "Protection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private unsafe void ButtonTryWriteBytesToPage_Click(object sender, EventArgs e)
        {
            var page = IntPtr.Add(handle, (int)_sys_info.dwPageSize * (int)numericUpDown.Value);

            try
            {
                for (int i = 0; i < (int)_sys_info.dwPageSize; ++i)
                {
                    Marshal.WriteByte(page, sizeof(byte) * i, 0);
                }

                MessageBox.Show("Success!", "Write bytes", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
            catch (AccessViolationException ex)
            {
                MessageBox.Show($"Error: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
    }
}
