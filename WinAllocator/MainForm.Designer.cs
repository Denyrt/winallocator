﻿namespace WinAllocator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExample1 = new System.Windows.Forms.Button();
            this.ButtonExample2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonExample1
            // 
            this.buttonExample1.Location = new System.Drawing.Point(45, 66);
            this.buttonExample1.Name = "buttonExample1";
            this.buttonExample1.Size = new System.Drawing.Size(188, 40);
            this.buttonExample1.TabIndex = 0;
            this.buttonExample1.Text = "Example1";
            this.buttonExample1.UseVisualStyleBackColor = true;
            this.buttonExample1.Click += new System.EventHandler(this.ButtonExample1_Click);
            // 
            // ButtonExample2
            // 
            this.ButtonExample2.Location = new System.Drawing.Point(45, 162);
            this.ButtonExample2.Name = "ButtonExample2";
            this.ButtonExample2.Size = new System.Drawing.Size(188, 40);
            this.ButtonExample2.TabIndex = 1;
            this.ButtonExample2.Text = "Example2";
            this.ButtonExample2.UseVisualStyleBackColor = true;
            this.ButtonExample2.Click += new System.EventHandler(this.ButtonExample2_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 40);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Example to continue";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(279, 50);
            this.label2.TabIndex = 3;
            this.label2.Text = "Simple virtual memory example";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(1, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(283, 64);
            this.label3.TabIndex = 4;
            this.label3.Text = "Virtual memory managment with threads, logging and file reports.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 278);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonExample2);
            this.Controls.Add(this.buttonExample1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WinAllocator";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonExample1;
        private System.Windows.Forms.Button ButtonExample2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}