﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinAllocator.Example2
{
    public enum Operations : uint
    {
        Reserve = 1,
        SetBlock = 2,
        DontSaveInPageFileAfterEdit = 3,
        FreeRegion = 4,
        ReturnBlock = 5,
        LockBlock = 6,
        UnlockBlock = 7
    }
}
