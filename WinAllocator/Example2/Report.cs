﻿using System;
using System.Text;
using WinAllocator.Core;

namespace WinAllocator.Example2
{
    public struct Report
    {
        public uint PageSize { get; }
        public uint Granularity { get; }
        public uint AvailablePhysMemory { get; }
        public uint AvailableVirtualMemory { get; }
        public uint AvailablePageFile { get; }
        public uint TotalPageFile { get; }

        public Report(in MEMORYSTATUS memoryStatus, in SYSTEM_INFO systemInfo)
        {
            PageSize = systemInfo.dwPageSize;
            Granularity = systemInfo.dwAllocationGranularity;
            AvailablePhysMemory = memoryStatus.dwAvailPhys;
            AvailableVirtualMemory = memoryStatus.dwAvailVirtual;
            AvailablePageFile = memoryStatus.dwAvailPageFile;
            TotalPageFile = memoryStatus.dwTotalPageFile;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(new string('-', 40) + Environment.NewLine);
            sb.Append($"PageSize:                   {PageSize};{Environment.NewLine}");
            sb.Append($"Granularity:                {Granularity};{Environment.NewLine}");
            sb.Append($"AvailablePhysMemory:        {AvailablePhysMemory};{Environment.NewLine}");
            sb.Append($"AvailableVirtualMemory:     {AvailableVirtualMemory};{Environment.NewLine}");
            sb.Append($"AvailablePageFile:          {AvailablePageFile};{Environment.NewLine}");
            sb.Append($"TotalPageFile:              {TotalPageFile};{Environment.NewLine}");            
            sb.Append(new string('-', 40) + Environment.NewLine);
            sb.Append(Environment.NewLine);
            return sb.ToString();
        }
    }
}
