﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAllocator.Example2
{
    public partial class ExampleForm2 : Form
    {        
        private Task _task;
        private Simulator _simulator;

        public ExampleForm2()
        {
            InitializeComponent();
        }

        private void ButtonFileReports_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() != DialogResult.OK) return;

            textBoxFileReports.Text = saveFileDialog.FileName;
        }

        private void ButtonFileCommands_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;

            textBoxFileCommands.Text = openFileDialog.FileName;
        }

        private void ToolStripButtonExecute_Click(object sender, EventArgs e)
        {            
            _task = new Task(Execute);
            _task.Start();
        }

        private async void Execute()
        {
            _simulator?.Dispose();
            _simulator = new Simulator(openFileDialog.OpenFile(), saveFileDialog.OpenFile());
            _simulator.Logging += Log;

            do
            {
                await _simulator.ReadAndExecuteCommand();
            }
            while (_simulator.CanExecute);

            MessageBox.Show("Finished.");
        }

        private void Log(object sender, string message)
        {
            var log = new Action<string>(richTextBox1.AppendText);

            if (InvokeRequired)
            {
                Invoke(log, message);
            }

            else
            {
                log(message);
            }
        }

        private void ExampleForm2_FormClosed(object sender, FormClosedEventArgs e)
        {
                
        }
    }
}
