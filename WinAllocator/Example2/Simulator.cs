﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using WinAllocator.Core;
using WinAllocator.Core.Enums;
using static WinAllocator.Core.WinApiExtensions;

namespace WinAllocator.Example2
{
    public class Simulator : IDisposable
    {
        /// <summary>
        /// Regions of memory.
        /// </summary>
        private readonly Dictionary<uint, IntPtr> _pages = new Dictionary<uint, IntPtr>();

        public StreamWriter ReportsStream { get; }
        public StreamReader CommandStream { get; }
        public IntPtr Pointer { get; private set; }        
        public bool CanExecute => _command != null;        
        public string LastErrorMessage { get; private set; }

        public event EventHandler<string> Logging;

        private string _command;
        private MEMORYSTATUS _memoryStatus;
        private SYSTEM_INFO _systemInfo;

        public Simulator(Stream reportsStream, Stream commandStream)
        {
            ReportsStream = new StreamWriter(commandStream);
            CommandStream = new StreamReader(reportsStream);
        }

        public void Dispose()
        {
            ReportsStream?.Dispose();
            CommandStream?.Dispose();
        }

        public async Task<bool> ReadAndExecuteCommand()
        {
            _command = await CommandStream.ReadLineAsync();            

            if (!CanExecute)
            {
                LastErrorMessage = "All commands was executed.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            // parse command and execute.
            // Time for await; number of region or block; operation (from 1 to 7); size in bytes; access;s
            var args = _command.Split('|');
            if (args.Length != 5)
            {
                LastErrorMessage = "Incorrect command arguments length.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            if (!double.TryParse(args[0], out var time) || time < 0)
            {
                LastErrorMessage = "Validation Error. Incorrect time value.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            if (!uint.TryParse(args[1], out var number))
            {
                LastErrorMessage = "Validation Error. Incorrect number of region/block value.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            
            if (!uint.TryParse(args[2], out var operationConst) && !Enum.IsDefined(typeof(Operations), operationConst))
            {
                LastErrorMessage = "Validation Error. Incorrect operation value.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            if (!uint.TryParse(args[3], out var bytes))
            {
                LastErrorMessage = "Validation Error. Incorrect size in bytes value.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            if (!Enum.TryParse(args[4], out MemoryProtection memoryProtection))
            {
                LastErrorMessage = "Validation Error. Incorrect MemoryProtection value.\n";
                Logging?.Invoke(this, LastErrorMessage);
                return false;
            }

            var operation = (Operations)operationConst;
            
            Logging?.Invoke(this, $"Wait {time}s ...\n");
            await Task.Delay(TimeSpan.FromSeconds(time));

            var contains = _pages.ContainsKey(number);
            var ptr = contains ? _pages[number] : IntPtr.Zero;

            switch (operation)
            {
                case Operations.Reserve:
                                      
                    ptr = VirtualAlloc(ptr, bytes, AllocationType.RESERVE, memoryProtection);
                    
                    if (!contains)
                    {
                        _pages.Add(number, ptr);
                    }
                    else
                    {
                        _pages[number] = ptr;
                    }

                    Logging?.Invoke(this, "Reserve\n");

                    break;

                case Operations.SetBlock:

                    ptr = VirtualAlloc(ptr, bytes, AllocationType.COMMIT, memoryProtection);

                    if (!contains)
                    {
                        _pages.Add(number, ptr);
                    }
                    else
                    {
                        _pages[number] = ptr;
                    }

                    Logging?.Invoke(this, "SetBlock\n");

                    break;

                case Operations.DontSaveInPageFileAfterEdit:

                    ptr = VirtualAlloc(ptr, bytes, AllocationType.DECOMMIT, memoryProtection);

                    if (!contains)
                    {
                        _pages.Add(number, ptr);
                    }
                    else
                    {
                        _pages[number] = ptr;
                    }

                    Logging?.Invoke(this, "DontSaveInPageFileAfterEdit\n");

                    break;

                case Operations.FreeRegion:

                    VirtualFree(ptr, 0, AllocationType.RELEASE);

                    if (!contains)
                    {
                        _pages.Add(number, ptr);
                    }
                    else
                    {
                        _pages[number] = ptr;
                    }

                    Logging?.Invoke(this, "FreeRegion\n");

                    break;

                case Operations.ReturnBlock:

                    ptr = VirtualAlloc(ptr, bytes, AllocationType.COMMIT, memoryProtection);

                    if (!contains)
                    {
                        _pages.Add(number, ptr);
                    }
                    else
                    {
                        _pages[number] = ptr;
                    }

                    Logging?.Invoke(this, "ReturnBlock\n");

                    break;

                case Operations.LockBlock:

                    MemoryProtection old = default;
                    var protect = VirtualProtect(ptr, (uint)Marshal.SizeOf(typeof(MEMORY_BASIC_INFORMATION)), MemoryProtection.READONLY, ref old);

                    Logging?.Invoke(this, "LockBlock\n");

                    break;

                case Operations.UnlockBlock:
                    
                    ptr = VirtualAlloc(ptr, bytes, AllocationType.COMMIT, memoryProtection);

                    if (!contains)
                    {
                        _pages.Add(number, ptr);
                    }
                    else
                    {
                        _pages[number] = ptr;
                    }

                    Logging?.Invoke(this, "UnlockBlock\n");

                    break;
            }


            GetSystemInfo(ref _systemInfo);
            GlobalMemoryStatus(ref _memoryStatus);

            var report = new Report(in _memoryStatus, in _systemInfo);
            await ReportsStream.WriteAsync(report.ToString());
            await ReportsStream.FlushAsync();
            Logging?.Invoke(this, "Report added.\n");

            return true;
        }        
    }
}
