﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinAllocator.Core
{
    public static class IntPtrExtensions
    {
        public static string ToAddressString(this IntPtr pointer)
        {
            return string.Format("0x{0}", pointer.ToString("x"));
        }
    }
}
