﻿using System;

namespace WinAllocator.Core
{
    public struct SYSTEM_INFO
    {
        public uint dwOemld;
        public uint dwPageSize;
        public IntPtr lpMinimumApplicationAddress;
        public IntPtr lpMaximumApplicationAddress;
        public uint dwActiveProcessorMask;
        public uint dwNumberOfProcessors;
        public uint dwProcessorType;
        public uint dwAllocationGranularity;
        public uint dwReserved;
    }
}
