﻿using System;
using WinAllocator.Core.Enums;

namespace WinAllocator.Core
{
    public struct MEMORY_BASIC_INFORMATION
    {
        public IntPtr BaseAddress;
        public IntPtr AllocationBase;
        public MemoryProtection AllocationProtect;
        public uint RegionSize;
        public AllocationType State;
        public MemoryProtection Protect;
        public MemoryType Type;

        public static readonly MEMORY_BASIC_INFORMATION Empty = default;
    }
}
