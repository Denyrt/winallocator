﻿using System;

namespace WinAllocator.Core.Enums
{
    [Flags]
    public enum AllocationType : uint
    {
        COMMIT                      =   0x00001000,  
        RESERVE                     =   0x00002000,  
        REPLACE_PLACEHOLDER         =   0x00004000,  
        RESERVE_PLACEHOLDER         =   0x00040000,  
        RESET                       =   0x00080000,  
        TOP_DOWN                    =   0x00100000,  
        WRITE_WATCH                 =   0x00200000,  
        PHYSICAL                    =   0x00400000,  
        ROTATE                      =   0x00800000,  
        DIFFERENT_IMAGE_BASE_OK     =   0x00800000,  
        RESET_UNDO                  =   0x01000000,  
        LARGE_PAGES                 =   0x20000000,  
        _4MB_PAGES                  =   0x80000000,
        _64K_PAGES                  =   LARGE_PAGES | PHYSICAL,
        UNMAP_WITH_TRANSIENT_BOOST  =   0x00000001,  
        COALESCE_PLACEHOLDERS       =   0x00000001, 
        PRESERVE_PLACEHOLDER        =   0x00000002,
        DECOMMIT                    =   0x00004000,
        RELEASE                     =   0x00008000,  
        FREE                        =   0x00010000 
    }
}
