﻿namespace WinAllocator.Core.Enums
{
    public enum MemoryType : uint
    {
        PRIVATE     =   0x00020000,
        MAPPED      =   0x00040000,  
        IMAGE       =   0x01000000  
    }
}
